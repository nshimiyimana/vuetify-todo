
import { reactive, readonly } from 'vue'

const state = reactive({
  counter: 0,
  colorCode: 'blue'
})

const methods = {
  decreaseCounter () {
    state.counter--
  },
  increaseCounter () {
    state.counter++
  },
  setColorCode (val) {
    state.colorCode = val
  }
}

const getters = {
  counterSquared () {
    return state.counter * state.counter
  }
}
// if casted read only to change this state modal in input not possible as in vuex to make this change after modify input we need computed and getter with setter funct
export default {
  state: readonly(state),
  methods,
  getters
}
